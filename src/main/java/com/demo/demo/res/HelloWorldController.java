package com.demo.demo.res;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @RequestMapping(value = "/hello-world", method = RequestMethod.GET)
    public String saySomeThing() {
        return "hello-world";
    }

    @RequestMapping(value = "/hello-world1", method = RequestMethod.GET)
    public String saySomeThing1() {
        return "hello-world1";
    }

    @RequestMapping(value = "/hello-world2", method = RequestMethod.GET)
    public String saySomeThing2() {
        return "hello-world2";
    }
}
